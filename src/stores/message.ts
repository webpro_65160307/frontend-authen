import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useMessageStore = defineStore('message', () => {
  const snackbar = ref(false)
  const text = ref('')
  const showMessage = function (meg: string) {
    text.value = meg
    snackbar.value = true
  }
  return { showMessage,snackbar,text }
})
